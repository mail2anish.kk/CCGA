var backslider = function backslider() {
	var movementOffset;
	
	/*Registering events for swipe left and right*/
	$(document).on('swipeleft', '.content', function(ev) {
		ev.stopPropagation();
		slide('left');
	});
	$(document).on('swiperight', '.content', function(ev) {
		ev.stopPropagation();
		slide('right');
	});

	/*Slide function called when user swipes left or right*/
	var slide = function(direction) {
		if (direction == 'left') {
			$('.slidercontent').animate({
				'left': '-=' + movementOffset
			}, 'slow', 'linear', function() {
				$('.slider li:last').after($('.slider li:first'));
				$('.slidercontent').css({
					'left': -movementOffset
				});
			});
		} else if (direction == 'right') {
			$('.slidercontent').animate({
				'left': '+=' + movementOffset
			}, 'slow', 'linear', function() {
				$('.slider li:first').before($('.slider li:last'));
				$('.slidercontent').css({
					'left': -movementOffset
				});
			});
		}
	}

	/*Dom ready event for initial calucations and positioning*/
	$(document).ready(function() {
		$('.slidercontent li').each(function() {
			$(this).css('background-image', 'url("' + $(this).data('url') + '")');
		});
		movementOffset = $('.slider').width();
		$('.slider li:first').before($('.slider li:last'));
		$('.slidercontent').css({
			'left': -movementOffset
		});
		$('.slidercontent li').css('width', movementOffset);
	});
}();